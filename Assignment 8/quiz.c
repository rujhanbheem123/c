#include <stdio.h>
struct students {
	char nam[100];
	char sub[100];
	float mark;
};

int main(){

	struct students stu[100];

	for(int i=0;i<5;i++){

		printf("Enter the name of the student: ");
		scanf("%s",&stu[i].nam);
		printf("subject: ");
		scanf("%s",&stu[i].sub);
		printf("Marks: ");
		scanf("%f",&stu[i].mark);
		printf("\n");
	}

	printf("\n --- Students Details --- \n");
	printf("\n");

	for(int i=0;i<5;i++){

		printf("Name: %s \n",stu[i].nam);
		printf("Subject: %s \n",stu[i].sub);
		printf("Marks: %.4f \n",stu[i].mark);
		printf("\n");
	}

}