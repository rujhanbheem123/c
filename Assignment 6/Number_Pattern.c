#include <stdio.h>
int z=1;

void patternrow(int b) {
    if(b>0) {
        printf("%d",b);
        patternrow(b-1);

    }
}

void pattern(int a) {
    if(a>0) {
        patternrow(z);
        printf("\n");
        z++;
        pattern(a-1);
    }

}


int main() {
    int a;
    printf("Enter Number to pattern: ");
    scanf("%d", &a);
    pattern(a);
    return 0;
}