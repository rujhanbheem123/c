#include<stdio.h>

int Attendees(int), Income(int), Cost(int), Profit(int);

//P=price

int	Attendees (int P){
	return 120-(P-15)/5*20;
}

int Income (int P){
	return Attendees(P)*P;
}

int Cost(int P){
	return Attendees(P)*3+500;
}

int Profit(int P){
	return Income(P)-Cost(P);
}

int main(){
	int P;
	printf("\nExpected Profit for Ticket Prices: \n\n");
	for(P=5;P<50;P+=5)
	{
		printf("Ticket Price = Rs.%d ------------- Profit = Rs.%d\n",P,Profit(P));
		printf("-----------------------------------------------------\n\n");
    }
		return 0;
}
