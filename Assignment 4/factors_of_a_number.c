#include <stdio.h>
int main() {
    int num, n;
    printf("Enter a integer: ");
    scanf("%d", &num);
    printf("The factors of %d are: \n", num);
    for (n = 1; n <= num; ++n) {
        if (num % n == 0) {
            printf("%d\n", n);
        }
    }
    return 0;
}